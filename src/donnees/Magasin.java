package donnees;


import java.util.ArrayList;
import java.util.List;


/**
 * La classe Magasin repr�sente un magasin qui vend des CDs.</p>
 * 
 * cette classe est caract�ris�e par un ensemble de CDs correspondant aux CDS vendus dans ce magasin.
 * 
 */
public class Magasin {

	/**
	 * la liste des CDs disponibles en magasin
	 */
	private ArrayList<CD> listeCds;

	/**
	 * construit un magasin par defaut qui ne contient pas de CD
	 */
	public Magasin() {
		listeCds = new ArrayList<CD>();
	}

	/**
	 * ajoute un cd au magasin
	 * 
	 * @param cdAAjouter
	 *            le cd � ajouter
	 */
	public void ajouteCd(CD cdAAjouter) {
		listeCds.add(cdAAjouter);
	}

	@Override
	/**
	 * affiche le contenu du magasin
	 */
	public String toString() {
		String chaineResultat = "";
		for (CD cd : listeCds) {
			chaineResultat += cd;
		}
		chaineResultat += "nb Cds: " + listeCds.size();
		return (chaineResultat);

	}

	// TODO ajouter m�thode de tri
	
	/**
	 * permet de trier par nom d'artistes
	 */
	public void trier(ComparateurCD comp) {
		// tri par selection
		int nbCDs = this.listeCds.size();
		for (int i = 0; i < nbCDs; i++) {
			CD cdSelectionne=this.listeCds.get(i);
			int indiceSelection=i;
			for (int j=i+1;j<nbCDs;j++)
			{
				CD cdTemp = listeCds.get(j);
				if (comp.estAvant(cdSelectionne, cdTemp))
				{
					indiceSelection=j;
					cdSelectionne=cdTemp;
				}
			}
			listeCds.set(indiceSelection,listeCds.get(i));
			listeCds.set(i,cdSelectionne);
		}
	}

	
	/**
	 * permet de trier par nom d'album
	 */
	public void trierAlbum() {
		// tri par selection
		int nbCDs = this.listeCds.size();
		for (int i = 0; i < nbCDs; i++) {
			CD cdSelectionne=this.listeCds.get(i);
			int indiceSelection=i;
			for (int j=i+1;j<nbCDs;j++)
			{
				CD cdTemp = listeCds.get(j);
				if (cdTemp.etreAvantAlbum(cdSelectionne))
				{
					indiceSelection=j;
					cdSelectionne=cdTemp;
				}
			}
			listeCds.set(indiceSelection,listeCds.get(i));
			listeCds.set(i,cdSelectionne);
		}
	}


	public List getListeCds() {
		return listeCds;
	}
	
	public CD getCD(String artiste, String nomCD) {
		for (CD cd : this.listeCds) {
			if (cd.getNomArtiste().equals(artiste)
					&& cd.getNomCD().equals(nomCD)) {
				return cd;
			}
		}
		return null;
	}
	// FinTODO ajouter une m�thode de tri

	
}
