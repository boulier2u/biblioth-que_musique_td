package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import donnees.CD;
import donnees.ComparateurAlbum;
import donnees.ComparateurArtiste;
import donnees.ComparateurCD;
import donnees.Magasin;
import XML.ChargeurMagasin;

import java.util.ArrayList;
public class Test_Magasin {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}
	
	@Test(expected = NullPointerException.class)
	public void test_mauvais_fichier() throws IOException{
		ChargeurMagasin ch = new ChargeurMagasin("Test");
		ch.chargerMusiques();	
		}
	

	@Test
	public void test_ChargeurMagasin() throws IOException{
		ChargeurMagasin ch = new ChargeurMagasin("../Ressources");
		Magasin m= ch.chargerMusiques();
		CD cd1 = m.getCD("Bénabar", "Bénabar");
		assertEquals("Le premoer nom d'artiste devrait etre a bénabar","Bénebar", cd1.getNomArtiste());
		assertEquals("Le premoer nom d'artiste devrait etre a bénabar","Bénebar", cd1.getNomCD());
		
		}
	
	
	@Test
	public void test_TriCd() throws IOException{
		ChargeurMagasin ch = new ChargeurMagasin("../../Ressources");
		Magasin m = ch.chargerMusiques();
		ComparateurAlbum c = new ComparateurAlbum();
		m.trier(c);
		
		assertEquals("Mal trier mauvais auteur","Bénabar" ,((CD) m.getListeCds().get(0)).getNomCD());
		assertEquals("Mal trier mauvais auteur","" ,((CD) m.getListeCds().get(m.getListeCds().size()-1)).getNomCD());
	
	}
	@Test
	public void test_Artiste() throws IOException{
		ChargeurMagasin ch = new ChargeurMagasin("../magasinCD_donnees/musicbrainzSimple/");
		Magasin m= ch.chargerMusiques();
		ComparateurArtiste c = new ComparateurArtiste();
		m.trier(c);
		System.out.println();
		assertEquals("Mal trier mauvais auteur","Bénabar" ,((CD) m.getListeCds().get(0)).getNomArtiste());
		assertEquals("Mal trier mauvais auteur","Zebda" ,((CD) m.getListeCds().get(m.getListeCds().size()-1)).getNomArtiste());
	
	}
	
	
		
	

	

}
